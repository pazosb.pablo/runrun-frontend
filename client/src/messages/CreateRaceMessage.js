import React from 'react'
import { useSelector } from 'react-redux'

const CreateRaceMessage = () => {
  const event = useSelector(s => s.event)
  return (
    <div>
      <div>Has creado una nueva carrera: <b>{event.type.event_name}</b></div>
      <div> Ahora puedes editarla o introducir participantes pulsando en el botón <b>cargar participantes</b></div>
    </div>
  )
}

export default CreateRaceMessage