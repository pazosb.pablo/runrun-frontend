import React from 'react'
import { useSelector } from 'react-redux'

const EmailSentMessage = () => {
  const participant = useSelector(s => s.participant)
  return (<div>Has enviado un email de recogida de dorsal a <b>{participant.type.participant_name} {participant.type.surname}</b></div>)
}

export default EmailSentMessage