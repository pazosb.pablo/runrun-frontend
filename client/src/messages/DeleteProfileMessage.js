import React from 'react'

const DeleteProfileMessage = () => {
  return (
    <div>
      <div>¡Acabas de eliminar tu perfil de RUN-RUN!</div>
      <div>Si deseas volver a usar la aplicación pulsa en ENTRAR y crea un nuevo perfil</div>
    </div>
  )
}

export default DeleteProfileMessage