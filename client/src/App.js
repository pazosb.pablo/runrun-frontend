import React from 'react';
import { BrowserRouter as Router, Switch, Route, useHistory } from 'react-router-dom'
import { Provider, useSelector, useDispatch } from 'react-redux'
import Modals from './home/Modals'
import generateStore from './reducers'
import Private from './private/Private'
import Home from './Home'
import Participants from './private/Participants'
import Contact from './private/Contact'
import './App.css';

const Content = () => {
  const user = useSelector(s => s.user)
  const dispatch = useDispatch()
  const history = useHistory()

  const logout = () => {
    dispatch({type: 'hideMessage'})
    dispatch({ type: 'logout' })
    history.push('/')
  }

  const contact = () => {
    history.push('/private/contact')
  }

  const handleEditProfile = () => dispatch({ type: 'showModal', modalType: 'editProfile' })

  const handleDeleteProfile = () => dispatch({ type: 'showModal', modalType: 'deleteProfile'})

  return (
    <div className="App">
      <header className="App-header">
        {user && <div></div>}
        <h1 className='title'>RUN-RUN</h1>
        {user &&
          <div className="App-header-content">
            <h4>
              <div id="user-name">¡Bienvenid@ {user.userInfo.organizer_name}!</div>
              <div className="dropdown">
                <ul id="burguer">
                  <li id="first"></li>
                  <li></li>
                  <li></li>
                </ul>
                <div id="menu">
                  <button onClick={handleEditProfile}>Editar Perfil</button>
                  <button onClick={handleDeleteProfile}>Eliminar Perfil</button>
                  <button onClick={logout}>Cerrar Sesión</button>
                  <button onClick={contact}>Contacto</button>
                </div>
              </div>
            </h4>
          </div>}
      </header>
      <main>
        <Switch>
          <Route path="/private" exact appProps={user}>
            <Private />
          </Route>
          <Route path="/private/participants" exact appProps={user}>
            <Participants />
          </Route>
          <Route path="/private/contact" exact appProps={user}>
            <Contact />
          </Route>
          <Route path="/" exact >
            <Home />
          </Route>
        </Switch>
        <Modals />
        <footer>
          <p>©2020 por Dorsales Team para Hack a Boss </p>
        </footer>
      </main>
    </div>
  )
}

const store = generateStore()

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <Content />
      </Router>
    </Provider>
  )
}

export default App;
