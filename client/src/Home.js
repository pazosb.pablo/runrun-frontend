import React from 'react';
import { useDispatch } from 'react-redux'
import Message from './messages/Message'
import './Home.css'

const Home = () => {
  const dispatch = useDispatch()
  const handleSignin = () => {
    dispatch({type: 'hideMessage'})
    dispatch({ type: 'showModal', modalType: 'signin' })
  }

  return (
    <div className="home">
      <div id="home-message"><Message/></div>
      <h2 className="subtitle">Bienvenid@ al Gestor de Dorsales</h2>
      <button onClick={handleSignin}>ENTRAR</button>
    </div>
  )
}

export default Home