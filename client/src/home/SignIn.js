import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useDispatch } from 'react-redux'

const SignIn = () => {
  const dispatch = useDispatch()
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const history = useHistory()
  const handleSignUp = () => dispatch({ type: 'showModal', modalType: 'signup' })
  const [isError, setError] = useState(false)

  const handleSubmit = async (e) => {
    e.preventDefault()
    const loginData = { email, password }
    setError(false)
    try {
      const ret = await fetch('http://localhost:3000/login', {
        method: 'POST',
        body: JSON.stringify(loginData),
        headers: {
          'Content-Type': 'application/json'
          // 'Authorization': localStorage.getItem('token') // Esto en todas las llamadas autenticadas
        }
      });

      const {user, token} = await ret.json()
      if (user && token){
        dispatch({type: 'login', user, token})
      localStorage.setItem('token', token)
      dispatch({ type: 'hideModal' })
      history.push('/private')
      } else {
        
        dispatch({type: 'showMessage', messageType: 'noRegistered'})
      }
      

      //localStorage.setItem('token', data.token) // Esto solo en login, para guardar el token
    } catch (err) {
      console.warn('Error:', err)
      setError(true)
    }
  }

  return (
    <form className="login-form" onSubmit={handleSubmit}>
      <h3 className='modalHeading'>Iniciar sesión</h3>
      <div className="form-field-login">
        <label className="fields" htmlFor='email'>Email:</label>
        <input
          id='email'
          type='email'
          name="email"
          required
          value={email}
          onChange={e => setEmail(e.target.value)}
        />
      </div>
      <div className="form-field-login">
        <label htmlFor='pass'>Password:</label>
        <input
          id='pass'
          name="password"
          type="password"
          required
          value={password}
          onChange={e => setPassword(e.target.value)}
        />
      </div>
      {isError && <div className="noRegMessage"><b>No estás registrad@, regístrate para poder acceder al área privada</b></div>}
      <p>Si todavía no estás registrad@ pulsa <a href="#" onClick={handleSignUp}>aquí</a></p>

      <div className="buttonsContainer-login">
        <button className="initSesion">Inicia sesión</button>
      </div>
      
    </form>
  )
}

export default SignIn