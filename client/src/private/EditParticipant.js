import React from 'react'
import { useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom'

import { useDispatch, useSelector } from 'react-redux'

const EditParticipant = () => {

  const useFormField = () => {
    const [value, setValue] = useState('')
    return [value, e => setValue(e.target.value)]
  }

  const history = useHistory()
  const location = useLocation();
  const dispatch = useDispatch();
  const currentModal = useSelector(s => s.modal)
  const user = useSelector(s => s.user)
  const [participantName, setParticipantName] = useFormField()
  const [surname, setSurname] = useFormField()
  const [tutorEmail, setTutorEmail] = useFormField()
  const [color, setColor] = useFormField()
  const [number, setNumber] = useFormField()
  const [numberType, setNumberType] = useFormField()
  const [email, setEmail] = useFormField()
  const [team, setTeam] = useFormField()
  const [participant, setParticipant] = useState()
  const editParticipant = useSelector(s => s.participant);
  const { token } = useSelector(s => s.user.token)
  const [isError, setError] = useState(false)

  const handleSubmit = async (e) => {
    e.preventDefault()
    const participant = currentModal.extraData;
    participant.participant_name = participantName;
    participant.surname = surname;
    participant.color = color;
    participant.email = email;
    try {
      const ret = await fetch('http://localhost:3000/participant', {
        method: 'PUT',
        body: JSON.stringify(participant),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + token
        }
      })
      dispatch({ type: 'participant', participant });
      dispatch({ type: 'hideModal' })

    } catch (err) {
      console.warn('Error:', err)
      setError(true)
    }
  }

  return (
    <form className="login-form" onSubmit={handleSubmit}>
      <h3 className='modalHeading'>Cambia los datos del participante</h3>
      <div className="form-field">
        <label className="fields" for="name">Nombre:</label>
        <input
          id="name"
          type="text"
          name="name"
          required
          placeholder={currentModal.extraData.participant_name}
          value={participantName}
          onChange={setParticipantName}
        />
      </div>
      <div className="form-field">
        <label className="fields" for="surname">Apellidos:</label>
        <input
          id="surname"
          type="text"
          name="surname"
          required
          placeholder={currentModal.extraData.surname}
          value={surname}
          onChange={setSurname}
        />
      </div>
      <div className="form-field">
        <label className="fields" for='color'>Color:</label>
        <input
          id="color"
          type="text"
          name="color"
          required
          placeholder={currentModal.extraData.color}
          value={color}
          onChange={setColor}
        />
      </div>
      <div className="form-field">mail
        <label className="fields" for="email">Email:</label>
        <input
          id="email"
          type="email"
          name="email"
          required
          placeholder={currentModal.extraData.email}
          value={email}
          onChange={setEmail}
        />
      </div>
      <div className="buttonsContainer">
        <button type="submit">Cambiar datos</button>
      </div>
      {isError && <div>Error, por favor inténtelo de nuevo</div>}
    </form >
  )
}

export default EditParticipant