import React from 'react'
import { useHistory } from 'react-router-dom'
import { useSelector } from 'react-redux'
import './Contact.css'


const Contact = () => {
  const user = useSelector(s => s.user)
  const history = useHistory()
  const volver = () => {
    history.push('/private')
  }

  return (
    <div className="contact-background">
      <div className="contact">
        <p className="primer">{user.userInfo.organizer_name}, puedes contactar con nosotros en la siguiente dirección:</p>
        <p className="segundo">info@runrun.org.es</p>
        <button onClick={volver}>Volver</button>

      </div>
    </div>
  )
}

export default Contact