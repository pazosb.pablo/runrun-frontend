import React from 'react';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../messages/Message'
import './Private.css';

const Private = () => {
  const useFormField = () => {
    const [value, setValue] = useState('')
    return [value, e => setValue(e.target.value)]
  }

  const dispatch = useDispatch()
  const history = useHistory()
  const user = useSelector(s => s.user)
  const event = useSelector(s => s.event)
  const [events, setEvents] = useState();
  const [raceName, setRaceName] = useFormField()
  const [council, setcouncil] = useFormField()
  const [raceDate, setRaceDate] = useFormField()
  const [isError, setError] = useState(false);
  const [searched, setSearched] = useState(false);

  const onInit = async () => {
    if (!user) {
      history.push('/');
    } else {
      setSearched(false);
      const ret = await fetch(`http://localhost:3000/events?idorganizer=${user.userInfo.id_organizer}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + user.token // Esto en todas las llamadas autenticadas
        }
      })
      const events = await ret.json()
      setEvents(events)
    }
  }

  const handleCrearCarrera = () => {
    dispatch({ type: 'hideMessage' })
    dispatch({ type: 'showModal', modalType: 'event' })
  }

  const handleVerParticipantes = (event) => {
    dispatch({ type: 'hideMessage' })
    history.push('/private/participants?idevent=' + event.id_event)
  }

  const handleBorrarCarrera = (event) => {
    dispatch({ type: 'hideMessage' })
    dispatch({ type: 'showModal', modalType: 'deleteRaceAlarm', 'extraData': event })
    onInit()
  }

  const handleSearch = async (e) => {
    e.preventDefault()
    dispatch({ type: 'hideMessage' });
    try {
      const ret = await fetch(`http://localhost:3000/events?idorganizer=${user.userInfo.id_organizer}&name=${raceName}&place=${council}&date=${raceDate}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + user.token
        }
      })
      const events = await ret.json()
      setEvents(events);
      setSearched(true);

    } catch (err) {
      console.error('Error:', err)
      setError(true)
    }
  }

  const handleCargaParticipantes = (event) => {
    dispatch({ type: 'hideMessage' })
    dispatch({ type: 'showModal', modalType: 'loader', 'extraData': event })
  }

  const handleEditarCarrera = (event) => {
    dispatch({ type: 'hideMessage' })
    dispatch({ type: 'showModal', modalType: 'editRace', 'extraData': event })
  }

  const backToEvents = () => {
    onInit();
    setSearched(false);
  }

  useEffect(() => {
    onInit();
  }, [user, event]);

  const selectedEvent = (event) => {
    dispatch({ type: 'hideMessage' })
    dispatch({ type: 'event' })
  }

  if (events && events.length > 0) {

    return (
      <div className="private">
        <div className="private-container fadeIn">
          <form className="private-login-form bounceInLeft">
            <div className="private-form-field">
              <label className="fields" htmlFor="name">Nombre de la carrera:</label>
              <input
                id="raceName"
                type="text"
                name="raceName"
                value={raceName}
                onChange={setRaceName}
              />
            </div>
            <div className="private-form-field">
              <label className="fields" htmlFor="surname">Lugar:</label>
              <input
                id="council"
                type="text"
                name="council"
                value={council}
                onChange={setcouncil}
              />
            </div>
            <div className="private-form-field">
              <label className="fields" htmlFor='email'>Fecha de la carrera:</label>
              <input
                id="raceDate"
                type="date"
                name="raceDate"
                value={raceDate}
                onChange={setRaceDate}
              />
            </div>
            <div className="private-Buscar">
              <button onClick={handleSearch}>Buscar</button>
            </div>
          </form >
        </div>
        <div className="fadeInUpBig">
          <div><Message /></div>
          <div className="events">
            <table>
              <thead>
                <tr>
                  <th className="smallcell">
                  </th >
                  <th>Nombre</th>
                  <th>Lugar</th>
                  <th>Fecha</th>
                  <th>Hora</th>
                  <th className="smallcell"></th >
                  <th className="smallcell"></th>
                  <th className="smallcell"></th>
                </tr>
              </thead>
              <tbody>
                {events.map(item => (
                  <tr className="table-row" onClick={selectedEvent}>
                    <td className="smallcell right"><button onClick={() => handleEditarCarrera(item)}>editar carrera</button></td>
                    <td>{item.event_name}</td>
                    <td>{item.place}</td>
                    <td>{item.event_date}</td>
                    <td>{item.start_time}</td>
                    <td className="smallcell right load"><button onClick={() => handleCargaParticipantes(item)}>cargar participantes</button></td>
                    <td className="smallcell right show"><button onClick={() => handleVerParticipantes(item)}>ver participantes</button></td>
                    <td className="smallcell right" id="delete"><button onClick={() => handleBorrarCarrera(item)}>borrar carrera</button></td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
          <div className="botonera">
            <button onClick={handleCrearCarrera}>Crear carrera</button>
            <button className={searched ? "inSearch" : "noSearch"} onClick={backToEvents}>Volver a la lista</button>
          </div>
        </div>
        {isError && <div>Error, por favor inténtelo de nuevo</div>}
      </div>
    )
  } else {
    return (
      <div className="private">
        <div className="private-container fadeIn">
          <form className="private-login-form bounceInLeft">
            <div className="private-form-field">
              <label className="fields" htmlFor="name">Nombre de la carrera:</label>
              <input
                id="raceName"
                type="text"
                name="raceName"
                value={raceName}
                onChange={setRaceName}
              />
            </div>
            <div className="private-form-field">
              <label className="fields" htmlFor="surname">Lugar:</label>
              <input
                id="council"
                type="text"
                name="council"
                value={council}
                onChange={setcouncil}
              />
            </div>
            <div className="private-form-field">
              <label className="fields" htmlFor='email'>Fecha de la carrera:</label>
              <input
                id="raceDate"
                type="date"
                name="raceDate"
                value={raceDate}
                onChange={setRaceDate}
              />
            </div>
            <div className="private-Buscar">
              <button onClick={handleSearch}>Buscar</button>
            </div>
          </form >
        </div>
        <div className="fadeInUpBig">
          <div><Message /></div>
          <div className="greetings">
            Bienvenid@ al área privada del gestor de dorsales, aquí podrás crear carreras pulsando en el botón <b>Crear Carrera</b>, y si tienes muchas carreras puedes encontrarlas rápidamente usando el buscador de arriba.
        </div>

          <div className="events"></div>
          <div className="botonera">
            <button className="btn-race" onClick={handleCrearCarrera}>
              Crear carrera
            </button>
          </div>
        </div>
        {isError && <div>Error, por favor inténtelo de nuevo</div>}
      </div>
    )
  }
}

export default Private

